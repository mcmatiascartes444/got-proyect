import { ICharacter } from '../types/character';
import https from 'https';
import { InternalError } from '../errors/internalError';


interface ExternalCharacter extends ICharacter {
  _id?: string;
}

export class Character {
  static async download(): Promise<ExternalCharacter[]> {
    return new Promise((resolve, reject) => {
      let body: string = "";
      const URL_API = process.env.URL_API || 'https://api.got.show/api/book/characters';
      https.get(URL_API, response => {
        if (response.statusCode && (response.statusCode < 200 || response.statusCode >= 300)) {
          return reject(new InternalError(`Invalid status code downloading characters ${response.statusCode}`))
        };
        response.on('data', chunk => {
          body += chunk.toString();
        });
        response.on('end', () => {
          try {
              return resolve(JSON.parse(body));
          } catch(e) {
            return reject(e);
          }
        });
      }).on('error', e => {
        return reject(e);
      });
    });
  }
}
