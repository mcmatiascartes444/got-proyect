import mongoose from 'mongoose';
import { ICharacter, IPageRank } from '../types/character';

interface CharacterDoc extends mongoose.Document {
  id?: string;
  externalId: string;
  titles: string[],
  books: string[],
  name: string,
  slug: string,
  gender: string,
  image: string,
  house: string,
  pagerank: IPageRank;
}

const characterSchema =  new mongoose.Schema({
  externalId: {
    type: String,
    required: true,
    unique: true
  },
  titles: {
    type: [String],
    required: true
  },
  books: {
    type: [String],
    required: true
  },
  name: {
    type: String,
    required: true,
    trim: true
  },
  slug: {
    type: String,
    required: true
  },
  gender: {
    type: String,
    required: false
  },
  image: {
    type: String,
    required: false
  },
  house: {
    type: String,
    required: false,
    trim: true,
    default: ''
  },
  pagerank: {
    title: {
      type: String,
      required: false
    },
    rank: {
      type: Number,
      required: false
    }
  }
}, {
  toJSON: {
      transform(_, ret) {
          ret.id = ret._id;
          delete ret._id;
          delete ret.__v;
      }
  }
});

characterSchema.index({ name: 'text', house: 'text' });
interface CharacterModel extends mongoose.Model<CharacterDoc> {
  build(attrs: ICharacter): CharacterDoc;
}

const CharacterModel = mongoose.model<CharacterDoc, CharacterModel>('Character', characterSchema);

export { CharacterModel, CharacterDoc };
