export interface IPageRank {
    title: string,
    rank: number
  }
  
  export interface ICharacter {
    id?: string;
    externalId?: string;
    titles: string[],
    books: string[],
    name: string,
    slug: string,
    gender: string,
    image: string,
    house: string,
    pagerank: IPageRank;
  }
  