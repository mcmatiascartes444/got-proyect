import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import { app } from './app';
import { Character as CharacterService } from './service/character';
import { CharacterModel } from './models/character';

const start = async () => {
  try {
    const mongo = new MongoMemoryServer();
    const mongoUri = await mongo.getUri();
    await mongoose.connect(mongoUri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true
    });

    console.log('connected to mongo db');

    const characters = await CharacterService.download();
    await CharacterModel.insertMany(characters.map(externalCharacter => {

      const character = {
        externalId: externalCharacter._id,
        ...externalCharacter,
      }

      delete character._id;

      return character;
    }));

    app.listen(3001, () => {
      console.log('Listening on 3001!');
  });
  } catch(err) {
    console.error(err);
  }
}

start();