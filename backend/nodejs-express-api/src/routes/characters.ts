import express, { Request, Response } from 'express';
import { param, query } from 'express-validator';
import { NotFoundError } from '../errors/notFoundError';
import { defaultPaginationValues } from '../middlewares/paginationBooks';
import { validateRequest } from '../middlewares/requestValidation';
import { CharacterModel } from '../models/character';

const router = express.Router();

router.get('/characters', defaultPaginationValues, [
  query('page').isDecimal(),
  query('limit').isDecimal(),
  query('text').optional({ nullable: true, checkFalsy: true }).trim().isLength({ min: 3 }),
], validateRequest, async (req: Request, res: Response) => {
  let limit = 250;
  let page = 1;
  if (req.query.limit) {
    limit = parseInt(req.query.limit.toString());
  }

  if (req.query.page) {
    page = parseInt(req.query.page.toString());
  }

  let query = {}

  if (req.query.text) {
    query = {
      '$text': {
        '$search': req.query.text.toString()
      }
    }
  }

  const charactersPage = await CharacterModel.find(query)
    .limit(limit)
    .skip((page - 1) * limit)
    .exec();

  const count = await CharacterModel.countDocuments(query);

  return res.json({
    page,
    totalPages: Math.ceil(count / limit),
    data: charactersPage
  });
});

router.get('/characters/:id', [param('id').isMongoId()], validateRequest, async (req: Request, res: Response) => {
  const { id } = req.params;
  const character = await CharacterModel.findById(id);

  if (!character) {
      throw new NotFoundError();
  }

  return res.send(character);
});

export { router as charactersRouter };
