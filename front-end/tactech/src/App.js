import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import CharacterInformation from './components/CharacterInformation.jsx';
import CharacterList from './components/CharacterList';

function App() {
  return (
    <Container>
      <Row>
        <Col>
          <Router>
            <Switch>
              <Route path="/character/:id">
                <CharacterInformation />
              </Route>
              <Route path="/">
                <CharacterList />
              </Route>
            </Switch>
          </Router>
        </Col>
      </Row>
    </Container>
  );
}

export default App;
