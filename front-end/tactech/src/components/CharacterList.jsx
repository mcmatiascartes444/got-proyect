import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Form , Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Pagination from '../components/Pagination';

const LIMIT_ELEMENTS = 10;


const CharacterList = () => {
  const [books, setBook] = useState();
  const [card, setCard] = useState();
  const [pageNumber, setPageNumber] = useState(1);
  const [searchText, setSearchText] = useState('');

  const searchData = async () => {
    const { data } = await axios.get(`http://localhost:3001/characters?page=${pageNumber}&limit=${LIMIT_ELEMENTS}&text=${searchText}`);
    console.warn(data)
    setBook(data)
    setCard(data.data);
  }

  useEffect(() => {
    searchData();
  }, [pageNumber]);

 
  return (
    <div className="App">
      <h1>Characters</h1>
      <h2>List from an API</h2>

      <Form>
            <Form.Group controlId="formSearch">
              <Form.Label>Search for name or house&nbsp;</Form.Label>
              <Form.Control
                onChange={e => {
                  e.preventDefault();
                  setSearchText(e.target.value);
                }}
                type="text"
                placeholder="Search text"
              />
            </Form.Group>
            <br></br>
            <Button
              onClick={() => {
                setPageNumber(1);
                searchData();
              }}
              className="button1"
              disabled={searchText.length !== 0 && searchText.length < 3}

            >
              Search
            </Button>
            </Form>

      <div className="books">
        {card &&
          card.map((book, index) => {
            const cleanedDate = new Date().toDateString();
            return (
              <div className="book" key={index}>
                <h2>{book.name}</h2>
                <div className="details">
                  <p>House: {book.house}</p>
                  <p>Slug: {book.slug}</p>
                  <p>Gender: {book.slug}</p>
                  <p>{cleanedDate}</p>
                  <Link to={`character/${book.id}`}>Más información</Link>
                </div>
              </div>
            );
          })}
      </div>
      {card &&
        <>
          <Pagination
            totalPages={books.totalPages}
            setPageNumber={setPageNumber}
            pageNumber={books.page}
            limit={LIMIT_ELEMENTS}
          />
        </>
      }

    


    </div>
  );
}

export default CharacterList;
