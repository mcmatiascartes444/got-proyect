import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Card } from 'react-bootstrap';
import Axios from 'axios';

const CharacterInformation = () => {
  const { id } = useParams();

  const [character, setCharacter] = useState({});

  useEffect(() => {
    const getData = async () => {
      const character = (await Axios.get(`http://localhost:3001/characters/${id}`)).data;
      console.warn(character)
      setCharacter(character);
    }
    getData();
  }, []);

  const { 
    gender, 
    slug, 
    image, 
    house, 
    name, 
    titles, 
    books, 
  } = character;

  return (
    <>
    { character &&
      <Card>
        <Card.Header></Card.Header>
        
        <div className="card-information"> 

        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Img variant="top" src={image} style={{
              "width": "40%",
              "height": "24vw"
            }}
          />
        </Card.Body>

            <p>Books: {books ? books.join(', ') : 'Empty Field'}</p> 
            <p>Titles: {titles ? titles.join(', ') : 'Empty Field'}</p>
            <p>{`Casa: ${house ? house : 'Empty Field'}`}</p>
            <p>{`Genero: ${gender ? gender : 'Empty Field'}`}</p>
            <p>{`Slug: ${slug}`}</p>        
        </div>
         
      </Card>
    }
    </>);
}

export default CharacterInformation;
