import React from 'react';
import { Pagination as BoostrapPagination } from 'react-bootstrap';

const Pagination = ({ setPageNumber, totalPages, pageNumber, limit }) => {
  return (
    <BoostrapPagination>
      <BoostrapPagination.First
        onClick={() => setPageNumber(1)}
        disabled={totalPages === 0}
      />

      <BoostrapPagination.Ellipsis
        onClick={() => setPageNumber(pageNumber - limit)}
        disabled={pageNumber - limit < 1}
      />

      {pageNumber - 2 >= 1 &&
        <BoostrapPagination.Item
          onClick={() => setPageNumber(pageNumber - 2)}
          disabled={pageNumber - 2 < 1}
        >
            {pageNumber - 2}
        </BoostrapPagination.Item>
      }
      {pageNumber - 1 >= 1 &&
        <BoostrapPagination.Item
          onClick={() => setPageNumber(pageNumber - 1)}
          disabled={pageNumber - 1 < 1}
        >
          {pageNumber - 1}
        </BoostrapPagination.Item>
      }

      <BoostrapPagination.Item active>{pageNumber}</BoostrapPagination.Item>

      <BoostrapPagination.Item
        onClick={() => setPageNumber(pageNumber + 1)}
        disabled={pageNumber + 1 > totalPages}
      >
        {pageNumber + 1}
      </BoostrapPagination.Item>
      <BoostrapPagination.Item
        onClick={() => setPageNumber(pageNumber + 2)}
        disabled={pageNumber + 2 > totalPages}
      >
        {pageNumber + 2}
      </BoostrapPagination.Item>

      <BoostrapPagination.Ellipsis
        onClick={() => setPageNumber(pageNumber + limit)}
        disabled={pageNumber + limit > totalPages}
      />

      <BoostrapPagination.Next
        onClick={() => setPageNumber(pageNumber + 1)}
        disabled={pageNumber + 1 > totalPages}
      />

      <BoostrapPagination.Last
        onClick={() => setPageNumber(totalPages)}
        disabled={totalPages === 0}
      />
    </BoostrapPagination>
  )
}

export default Pagination;
