import axios from "axios";

const cAxios = axios.create({
  baseURL: "http://localhost:3001/api/"
});

export const getAllCharacters = (page, search) => {
  return cAxios.get(`/characters/`, {params: {page, q: search}});
};

export const getCharacterById = (id) =>  {
  return cAxios.get(`/characters/${id}`);
}
